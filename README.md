# Test-technique


### Install Project

This project use ``vagrant`` and ``virtualbox``. 
To install them use link below:

[vagrant](https://www.vagrantup.com/downloads.html)

[virtualbox](https://www.virtualbox.org/wiki/Downloads)

Then you can clone and go to the repository.

Run command below into the repository to install environment
```
vagrant up

# For windows WSL users run
vagrant.exe up
```

### Run project

```
vagrant ssh
cd /vagrant
source .venv/bin/activate
python app/manage.py runserver 0:8000
```

Then go to `htpp://localhost:8000` to watch the project running.



### BONUS

This app can also run  a standalone Vue.JS app through Django and still be able to render 
VueJS integrated in the django templates.
This setup can be use to migrate step by step the django templates into a full app Vue.JS.

To try it you need two terminal into the virtualbox.
In one of them run:
```
python app/manage.py runserver 0:8000
```
And in the second run:
```
cd app/client
npm run serve
```
Then navigate to `http://127.0.0.1:8000/` to see the django template with vue rendered
and navigate to `http://127.0.0.1:8000/vue` to see the standalone Vue app.
As you can see both live alongside.