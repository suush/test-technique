# -*- mode: ruby -*-
# vi: set ft=ruby :

SHARE_ORIGIN = File.dirname(__FILE__)
SHARE_DEST = "/vagrant"
VAGRANTFILE_API_VERSION = "2"
PROJECT_NAME = "ipaidthat"

DEV_BOX = "ubuntu/xenial64"

def configure_box(config:, image: DEV_BOX)
  config.vm.box_check_update = true
  config.vm.box = image
  config.vm.hostname = "test-technique-sigrid"
  config.vm.boot_timeout = 800
  config.vm.network :private_network, ip: "10.10.10.61"
end

def configure_ssh(config:, host: "127.0.0.1", host_port: 2222, guest_port: 22)
  config.ssh.host = host
  config.ssh.port = host_port
  config.ssh.guest_port = guest_port
  config.ssh.forward_x11 = true
  config.ssh.pty = true
  config.ssh.keep_alive = true

end

def forward_ports(config:,id:,protocol: "tcp", guest_ip: "0.0.0.0", guest_port:, host_ip: "0.0.0.0",host_port:)
  config.vm.network "forwarded_port", id: id, protocol: protocol, guest_ip: guest_ip, guest: guest_port, host_ip: host_ip, host: host_port, auto_correct: true
end

def configure_share_folder(config:, shared_folder:  SHARE_ORIGIN, dest_folder:  SHARE_DEST)
  if !Vagrant.has_plugin?("vagrant-share")
    system('vagrant plugin install vagrant-share')
  end
  config.vm.synced_folder shared_folder, dest_folder, type: "virtualbox", create: true, group: "vagrant", owner: "vagrant"
end

def configure_virtualbox_providers(config:, name:, cpu:, memory:, image: DEV_BOX)
  if !Vagrant.has_plugin?("vagrant-vbguest")
    system('vagrant plugin install vagrant-vbguest')
  end
  config.vm.provider "virtualbox" do |vb|
    vb.gui = true
    vb.name = "[vagrant] #{name}"
    vb.memory = memory
    vb.cpus = cpu
    vb.linked_clone = true
    vb.customize ["setextradata", :id, "VBoxInternal2/SharedFoldersEnableSymlinksCreate/vagrant", "1"]
  end
end

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
 config.vm.define PROJECT_NAME, primary: true do |config|
    configure_box(config: config)
    configure_share_folder(config: config)
    configure_virtualbox_providers(config: config, name: PROJECT_NAME, cpu: 8, memory: 8192)
    forward_ports(config: config, id: "web", host_port: 80, guest_port: 80)
    forward_ports(config: config, id: "django", host_port: 8000, guest_port: 8000)
    forward_ports(config: config, id: "client", host_port: 8080, guest_port: 8080)
    config.vm.provision "shell", run: "always", path: "provision.sh", :args => SHARE_DEST
  end
end
