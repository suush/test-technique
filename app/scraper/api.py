from rest_framework import routers, serializers, viewsets, status
from scraper.models import Invoice
from scraper.scraper import Scrapper


class ScraperViewSet(viewsets.ModelViewSet):
    queryset = Invoice.objects.all()

    def post(self, credentials):
        try:
            scraper = Scrapper(credentials['email'], credentials['password'])
            _invoices = scraper.get_invoices_by_user()
            for item in _invoices:
                item.update( {"email":credentials['email']})
                Invoice.objects.update_or_create(**item)
        except Exception as e:
            return {'status_code': 500}
        else:
            return {'status_code': 200}

    @staticmethod
    def get(email=None):
        data = Invoice.objects.filter(email=email)
        return list(data.values())