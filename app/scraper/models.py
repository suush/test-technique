from django.db import models


class Invoice(models.Model):
    # Should be emailField and reformat from scraper value
    email = models.CharField(max_length=200)
    number = models.CharField(max_length=200)
    # Should be DateField and string from scrapper script must be reformat
    date = models.CharField(max_length=200)
    client_name = models.CharField(max_length=200)
    total_ttc = models.CharField(max_length=200)
    vat = models.CharField(max_length=200)
