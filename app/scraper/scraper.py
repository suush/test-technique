from __future__ import annotations
import argparse
import re
import requests
import typing

from bs4 import BeautifulSoup
from dataclasses import dataclass
from dataclasses import field
from requests.cookies import RequestsCookieJar
from pprint import pprint

if typing.TYPE_CHECKING:
    from requests.models import Response


@dataclass
class Scrapper:
    email: str
    password: str
    login_url: str = "https://app.factomos.com/controllers/app-pro/login-ajax.php"
    # login_url: str = "https://app.factomos.com/connexion"
    # invoices url could also be done with removing query parameter subfilter and parsing the DOM on "ITEM-NUMBER" field
    invoice_url: str = "https://app.factomos.com/mes-factures?&subFilter=valid"
    cookie: RequestsCookieJar = RequestsCookieJar()
    invoices_dom: typing.List[str] = field(default_factory=list)
    invoices: typing.List[typing.Dict[str, str]] = field(default_factory=list)


    def _scrap_invoices(self) -> typing.List[str]:
        with requests.Session() as session:
            data = {
                'appAction': 'login',
                'email': self.email,
                'password': self.password,
            }
            session.post(self.login_url, data=data)
            response = session.get(self.invoice_url)
            soup = BeautifulSoup(response.content, 'html.parser')
            self.invoices_dom = soup.find_all('tr', id=re.compile("^table-line-"))
            # print (self.invoices_dom)


    def _serialize_invoices(self):
        for invoice_dom in self.invoices_dom:
            self.invoices.append(
                {
                    'number': invoice_dom.find('input', type="checkbox")['value'],
                    'date': invoice_dom.find('td', "ITEM-DATE").contents[0].strip(),
                    'client_name': invoice_dom.find('td', "ITEM-CLIENT").a.contents[0],
                    'total_ttc': invoice_dom.find('td', "ITEM-TOT-TTC")['data-value'],
                    'vat': invoice_dom.find('td', "ITEM-TOT-TVA").contents[0]
                }
            )

    def get_invoices_by_user(self) -> typing.Optional[typing.List[str]]:
        self._scrap_invoices()
        self._serialize_invoices()
        return self.invoices


if __name__ == "__main__":
    '''
        This main could also be extract as a custom command for manual call or automated call.
    '''
    print("first")
    parser = argparse.ArgumentParser()
    parser.add_argument("--email")
    parser.add_argument("--password")
    args = parser.parse_args()
    scrap = Scrapper(email=args.email, password=args.password)
    scrap.get_invoices_by_user()
