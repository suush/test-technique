from django.urls import path

from . import views

urlpatterns = [
    path('', views.FormScraper, name='index'),
    path('invoices/', views.HistoricalInvoice, name='invoices'),
]