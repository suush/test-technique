from django.shortcuts import render, redirect
from scraper.api import ScraperViewSet


def FormScraper(request):
    if request.method == 'POST':
        response = ScraperViewSet.post(request, request.POST)
        if not response['status_code'] == 200:
            context = {'error_invoices':"There is an error during scrapping OR No data available for this user"}
            return render(request, 'vue_formScraper.html', context)
        else:
            return redirect(f'/invoices/?email={request.POST["email"]}')
    else:
        return render(request, 'vue_formScraper.html')


def HistoricalInvoice(request):
    context = {'items_json': ""}
    if request.GET['email']:
        data = ScraperViewSet.get(email=request.GET['email'])
        context = {'items_json': data}
    return render(request, 'vue_listScraper.html', context)
