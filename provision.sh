#!/bin/bash

# This file is specified in Vagrantfile and is loaded by Vagrant as the primary
# provisioning script whenever the commands `vagrant up`, `vagrant provision`,
# or `vagrant reload` are used. It provides all of the default packages and
# configurations included with Varying Vagrant Vagrants.
SHARED_FOLDER_PATH=$1

apt_package_install_list=(
curl
git
vim
tmux
python3.7
python3.7-pip
python3.7-dev
python3.7-venv
build-essential
make
nodejs
)

echo "Provisioning virtual machine..."

add-apt-repository ppa:deadsnakes/ppa
apt-get -qq update

if ! grep -q "alias la=\"ls -la\"" /home/vagrant/.bashrc; then
  echo "alias la=\"ls -la\"" >> /home/vagrant/.bashrc
fi

for pkg in "${apt_package_install_list[@]}"
do
    if [ "$pkg" == "nodejs" ]; then
        curl -sL https://deb.nodesource.com/setup_12.x | sudo bash -
    fi
    sudo apt-get install -q -y $pkg
done

apt-get update
apt-get upgrade -y
apt-get autoremove -y


echo "Install project dependencies ..."
cd $SHARED_FOLDER_PATH
if ! grep -q "export PYTHONPATH=/vagrant" /home/vagrant/.bashrc; then
    echo "export PYTHONPATH=/vagrant" >> /home/vagrant/.bashrc
    echo "export PYTHONPATH=.venv/lib/python3.7/site-packages/ " >> /home/vagrant/.bashrc
fi
python3.7 -m venv .venv
.venv/bin/pip3 install -qr requirements.txt
if [ -f "app/db.sqlite3" ]; then
    rm -rf app/db.sqlite3
fi
.venv/bin/python app/manage.py makemigrations
.venv/bin/python app/manage.py migrate
# install BONUS standalone vue app alongside Django template vue
echo "Bonus setup ..."
cd app/client
if [ -d "$SHARED_FOLDER_PATH/client/node_modules" ]; then
    rm -rf nodes_modules
fi

# Update path in order to use node package installed locally as executable
if ! grep -q "export  PATH=\"$PATH:/vagrant/client/node_modules/.bin\"" /home/vagrant/.bashrc; then
  echo "export  PATH=\"$PATH:/vagrant/client/node_modules/.bin\"" >> /home/vagrant/.bashrc
fi
npm install -g @vue/cli
npm i
